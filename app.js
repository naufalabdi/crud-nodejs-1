const path = require('path');
const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const { application } = require('express');
const app = express();

// mysql connection
const connection = mysql.createConnection({
    host : 'localhost',
    port : '3310',
    user : 'root',
    password : '123',
    database : 'node_crud_1'
});

// handle database connection error
connection.connect(e => {
    if (!!e) console.log(e);
    else console.log('Database Connected');
});

// set views file
app.set('views', path.join(__dirname, 'views'));

// set view engine
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Routes List
app.get('/', (req, res) => {
    // res.send('CRUD Operation using NodeJs / ExpressJs / MySQL');

    let sql = 'SELECT * FROM users';
    let query = connection.query(sql, (err, rows) => {
        if (err) throw err;
        res.render('user/index', {
            title: 'CRUD Operation using Node Js / ExpressJs / MySQL',
            users : rows
        });
    });
    
})

app.get('/add', (req, res) => {
    res.render('user/add', {
        title : 'Create new user'
    });
});

app.post('/save', (req, res) => {
    let data = {
        name : req.body.name,
        email : req.body.email,
        phone_no : req.body.phone
    };

    if (data.name.length === 0 || data.email.length === 0 || data.phone_no.length === 0 ) res.redirect('/add');

    let sql = "INSERT INTO users SET ?";
    let query = connection.query(sql, data, (err, results) => {
        if (err) throw err;
        res.redirect('/');
    });
});

app.get('/edit/:userId', (req, res) => {
    const userId = req.params.userId;
    let sql = `SELECT * FROM users where id = ${userId}`;
    let query = connection.query(sql, (err, result) => {
        if (err) throw err;
        res.render('user/edit', {
            title : `Detail User ${ userId }`,
            user : result[0]
        });
    });
});

app.post('/edit/:userId', (req, res) => {
    const userId = req.params.userId;
    let data = {
        name : req.body.name,
        email : req.body.email,
        phone_no : req.body.phone
    };
    
    let sql = `UPDATE users SET name='${ data.name }', email='${data.email}', phone_no='${ data.phone_no }' WHERE id = ${ userId }`;
    connection.query(sql, data, (err, result) => {
        if (err) throw err;
        res.redirect('/');
    });
});

app.get('/del/:userId', (req, res) => {
    const userId = req.params.userId;
    let sql = `DELETE FROM users where id = ${ userId }`;
    connection.query(sql, (err, result) => {
        if (err) throw err;
        res.redirect('/');
    })
});

// Server Listening 
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});